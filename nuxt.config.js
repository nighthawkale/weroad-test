export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "WeRoad test - Alessandro Marocchini",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: 'stylesheet', href: 'https://cdn.weroad.io/common/fonts/weroad-font.css' }
    ],
  },
  manifest: {
    name: "WeRoad test - Alessandro Marocchini",
    short_name: "WeRoad test - Alessandro Marocchini",
    description: "WeRoad test - Alessandro Marocchini",
    theme_color: "#2C3E50",
    start_url: "/",
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/css/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/tailwind-components.js" },
    {src: '~/service/toursService.js'},
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
    "@nuxtjs/moment",
    'nuxt-sass-resources-loader',
    "@nuxtjs/svg",
    'lodash'
  ],
  moment: {
    defaultTimezone: "Europe/Rome",
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    "@nuxt/image",
    ['@nuxtjs/dotenv', { filename: '.env.' + process.env.ENV }]
  ],
  // Nuxt Axios
  axios: {
    proxy:
      process.env.NODE_ENV === "production"
        ? false
        : process.env.NODE_ENV !== "staging",
    baseURL: process.env.BASE_URL || "http://localhost:80",
    // proxyHeaders: false,
    // credentials: false
  },
  proxy: {
    "/api/v1/": {
      target: "http://localhost:80/api/v1",
      pathRewrite: { "^/api/v1/": "" },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ["epic-spinners"],
  },
  loading: false,
  publicRuntimeConfig: {
    baseURL: process.env.BASE_URL || "http://localhost:80",
    nodeEnv: process.env.NODE_ENV || "development",
  },
};
