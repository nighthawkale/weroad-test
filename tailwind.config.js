const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  important: true,
  // Active dark mode on class basis
  darkMode: "class",
  i18n: {
    locales: ["it-IT"],
    defaultLocale: "it-IT",
  },
  purge: {
    content: [
      "./pages/**/*.tsx",
      "./components/**/*.tsx",
      "./components/**/*.{vue,js}",
      "./layouts/**/*.vue",
      "./pages/**/*.vue",
      "./plugins/**/*.{js,ts}",
      "./nuxt.config.{js,ts}",
    ],
    // These options are passed through directly to PurgeCSS
  },
  theme: {
    fontFamily: {
      body: ["proxima-nova"],
      title: ["Gilroy-ExtraBold"]
    },
    extend: {
      screens: {
        'xss': {'max': '320px'},
        'xs': {'min': '321px', 'max': '375px'},
        'mobile': {'min': '320px', 'max': '1024px'},
        ...defaultTheme.screens,
      },
      backgroundImage: (theme) => ({

      }),
    },
    zIndex: {
      0: 0,
      10: 10,
      20: 20,
      30: 30,
      40: 40,
      50: 50,
      25: 25,
      75: 75,
      90: 90,
      100: 100,
      auto: "auto",
    },
  },
  variants: {
    extend: {
      backgroundColor: ["active"],
      borderColor: ["active"],
      textColor: ['active']
    },
  },
  plugins: [require("@tailwindcss/forms")],
  future: {
    purgeLayersByDefault: true,
  },
};
