import countriesJson from '~/assets/json/countries.json'
import _ from 'lodash';
import * as context from "../vue.config";


export default class Utils {
  static filterTours(originalTours) {
    let filteredTours = [];
    countriesJson.map(country => {
      filteredTours[country.name] = originalTours.filter(tour => {
        return (tour.isActive && tour.primaryDestination.primaryContinent.name === country.name)
      })
    })
    return filteredTours;
  }

  static filterTrendTour(originalTours) {
    let filteredTours = [];
    filteredTours = _.orderBy(originalTours, ["bestTour.price"], ["asc"]);
    return filteredTours.slice(0, 8);
  }

  static log(msg, options) {
    if (context.DEBUG_LOG === true) console.log(msg);
  }

  static searchTourByTerms(tours,value,type) {
    let filteredTours = tours.filter(tour => {
      let tourTitle = tour.title.toLowerCase()
      const regex = new RegExp(value.trim().toLowerCase());
      return String(tourTitle).match(regex)
    });
    if (type==="trending-tours") {
      filteredTours = _.orderBy(filteredTours, ["bestTour.price"], ["asc"]);
      filteredTours = filteredTours.slice(0, 8);
    }
    return filteredTours;
  }

}
