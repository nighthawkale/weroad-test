import Vue from "vue";
import axios from "axios";
import * as context from "../vue.config";
import Utils from "@/utils/utils";

Vue.prototype.$getTours = async() => {
  Utils.log(context.ENDPOINT);
  let result = await axios.post(context.ENDPOINT + "/travels");
  return result.data;
};
