import axios from "axios";
import * as context from "../vue.config";
import Utils from "@/utils/utils";

export const state = () => ({
  country: null,
  tours: [],
  searchToursValue: null
})

// method to retrieve the data of the state objects
export const getters = {
  getCountrySelected: (state) => {
    return state.country
  },
}

// method to change the value of state objects
export const mutations = {
  setTours (state, tours) {
    state.tours = tours
  },
  setCountry (state, country) {
    state.country = country
  },
  setSearchToursValue (state, value) {
    state.searchToursValue = value
  },

}

// methods to call in components of the web app
// this method call mutations through context.commit
export const actions = {
  async setCountry ({ context, commit }, country) {
    commit('setCountry', country);
  },
  async setSearchToursValue ({ context, commit }, value) {
    commit('setSearchToursValue', value);
  },
  async getTours ({ context, commit }) {
    let app = await this._vm.$getTours();
    let tours = Utils.filterTours(app.data);
    commit('setTours', tours);
  },

}
