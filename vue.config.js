// vue.config.js
module.exports = {
  silent: true,
  configureWebpack: config => {
    config.output.globalObject = "this"
  },
  // ENDPOINT
  ENDPOINT: process.env.ENDPOINT,
  DEBUG_LOG: process.env.DEBUG_LOG,
  DEBOUNCE_TIME_SEARCH: 800
}
